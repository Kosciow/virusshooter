import java.util.ArrayList;

class Target {

    private ArrayList<String> polaPolozenia;
    private String nazwa;


    public void setPolaPolozenia(ArrayList<String> ppol) {
        polaPolozenia = ppol;
    }

    public void setNazwa(String virusName) {
        nazwa = virusName;
    }

    @Override
    public String toString() {
        return nazwa;
    }

    public String sprawdz(String ruch) {
        String wynik = "pudło";
        int indeks = polaPolozenia.indexOf(ruch);
        if (indeks >= 0) {
            polaPolozenia.remove(indeks);
            if (polaPolozenia.isEmpty()) {
                wynik = "zatopiony";
            } else {
                wynik = "trafiony";
            }
        }
        return wynik;
    }
}