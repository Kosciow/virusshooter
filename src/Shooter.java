import java.io.*;
import java.util.*;

public class Shooter {
    private static final String alfabet = "abcdefg";
    private int dlugoscPlanszy = 5;
    private int wielkoscPlanszy = 25;
    private int [] plansza = new int[wielkoscPlanszy];
    private int iloscVirusów = 0;
    private ArrayList<Target> listaVirusów = new ArrayList<Target>();
    private int iloscRuchow = 0;

    public String pobierzDaneWejsciowe(String komunikat) {
        String daneWejsciowe = null;
        System.out.print(komunikat + "  ");
        try {
            BufferedReader is = new BufferedReader(
                    new InputStreamReader(System.in));
            daneWejsciowe = is.readLine();
            if (daneWejsciowe.length() == 0 )  return null;
        } catch (IOException e) {
            System.out.println("IOException: " + e);
        }
        return daneWejsciowe.toLowerCase();
    }

    public ArrayList rozmieśćVirusa(int wielkośćVirusa) {
        ArrayList<String> zajetePola = new ArrayList<String>();
        String [] wspolrzedneLnc = new String [wielkośćVirusa];
        String pomoc = null;
        int [] wspolrzedne = new int[wielkośćVirusa];
        int prob = 0;
        boolean powodzenie = false;
        int polozenie = 0;

        iloscVirusów++;
        int inkr = 1;
        if ((iloscVirusów % 2) == 1) {
            inkr = dlugoscPlanszy;
        }

        while (!powodzenie & prob++ < 200 ) {
            polozenie = (int) (Math.random() * wielkoscPlanszy);
            int x = 0;
            powodzenie = true;
            while (powodzenie && x < wielkośćVirusa) {
                if (plansza[polozenie] == 0) {
                    wspolrzedne[x++] = polozenie;
                    polozenie += inkr;
                    if (polozenie >= wielkoscPlanszy){
                        powodzenie = false;
                    }
                    if (x>0 & (polozenie % dlugoscPlanszy == 0)) {
                        powodzenie = false;
                    }
                } else {

                    powodzenie = false;
                }
            }
        }

        int x = 0;
        int wiersz = 0;
        int kolumna = 0;
        while (x < wielkośćVirusa) {
            plansza[wspolrzedne[x]] = 1;
            wiersz = (int) (wspolrzedne[x] / dlugoscPlanszy);
            kolumna = wspolrzedne[x] % dlugoscPlanszy;
            pomoc = String.valueOf(alfabet.charAt(kolumna));

            zajetePola.add(pomoc.concat(Integer.toString(wiersz)));
            x++;

        }


        return zajetePola;
    }


    public void przygotujGre() {
        Target pierwszy = new Target();
        pierwszy.setNazwa("Czarnobyl");
        Target drugi = new Target();
        drugi.setNazwa("Morris Internet Worm");
        Target trzeci = new Target();
        trzeci.setNazwa("Mariposa");
        listaVirusów.add(pierwszy);
        listaVirusów.add(drugi);
        listaVirusów.add(trzeci);

        System.out.println("Twoim celem jest zatopienie trzech virusów komputerowych:");
        System.out.println(listaVirusów.toString());
        System.out.println("Postaraj się je zatopić w jak najmniejszej ilości ruchów. Plansza jest rozmiaru: "+dlugoscPlanszy+" na "+dlugoscPlanszy+".");
        System.out.println("Przykładowe podanie współrzędnej a4.");
        for (Target rozmieszczanyTarget : listaVirusów) {
            ArrayList<String> nowePolozenie = rozmieśćVirusa(3);
            rozmieszczanyTarget.setPolaPolozenia(nowePolozenie);
        }
    }



    public void rozpocznijGre() {
        while(!listaVirusów.isEmpty()) {
            String ruchGracza = pobierzDaneWejsciowe("Podaj pole:");
            sprawdzRuchGracza(ruchGracza);
        }
        zakonczGre();
    }

    private void sprawdzRuchGracza(String ruch) {
        iloscRuchow++;
        String wynik  = "pudło";
        for (Target targetDoSprawdzenia : listaVirusów) {
            wynik = targetDoSprawdzenia.sprawdz(ruch);
            if (wynik.equals("trafiony")) {
                break;
            }
            if (wynik.equals("zatopiony")) {
                listaVirusów.remove(targetDoSprawdzenia);
                break;
            }
        }
        System.out.println(wynik);
    }

    private void zakonczGre() {
        System.out.println("Wszystkie virusy zostały zatopione!");
        if (iloscRuchow <= 18) {
            System.out.println("Wykonałeś " + iloscRuchow + " ruchów.");
            System.out.println("Gratulacje");
        } else {
            System.out.println("Nie poszło! Wykonałeś aż "+ iloscRuchow + " ruchów.");
            System.out.println("Przykro mi :(");
        }
    }

}